Test work for recruitment to HRtec
================

[![License](http://img.shields.io/badge/license-MIT-lightgrey.svg)](https://github.com/SammyK/package-skeleton/blob/master/LICENSE)


Preparing RSS / Atom fetch in PHP

The aim of the task is to write a simple PHP program run from CLI (Windows CMD, Unix
Shell) which will retrieve RSS / Atom data from a given URL and save them to a * .csv file
The written program is supposed to execute 2 commands.

- [Installation](#installation)
- [Usage](#usage)
- [Testing](#testing)
- [Credits](#credits)
- [License](#license)


Installation
------------
Download repository and install composer packages use:

``` bash
$ composer install --no-dev
```

Usage
-----
1. simple - download RSS/Atom data from the URL and save it to the .csv file. Old data from the .csv file must be overwritten.
``` bash
$  php src/console.php csv:simple https://blog.nationalgeographic.org/rss feed.csv
```

2. extended - download RSS/Atom data from the URL and add it to the .csv file. Old data from the .csv file must be saved.
``` bash
$  php src/console.php csv:extended https://blog.nationalgeographic.org/rss feed.csv
```

Testing
-------
1. The PHP Coding Standards Fixer (PHP CS Fixer) tool fixes your code to follow standards: whether you want to follow PHP coding standards as defined in the PSR-1, PSR-2, etc., or other community driven ones like the Symfony one. You can also define your (team's) style through configuration.
``` bash
$ vendor/bin/php-cs-fixer fix . -v --using-cache=no --path-mode=intersection  --dry-ru
```

2. PHPStan focuses on finding errors in your code without actually running it. It catches whole classes of bugs even before you write tests for the code. It moves PHP closer to compiled languages in the sense that the correctness of each line of the code can be checked before you run the actual line.
``` bash
$ vendor/bin/phpstan analyse -c phpstan.neon
```

3. Test for project
``` bash
$ vendor/bin/phpunit
```

Credits
-------

- [Dmytro Staroselskyi](https://bitbucket.org/dstaroselskiy)


License
-------

The MIT License (MIT). Please see [License File](https://bitbucket.org/dstaroselskiy/rekrutacja-hr-tec/src/main/LICENSE) for more information.
