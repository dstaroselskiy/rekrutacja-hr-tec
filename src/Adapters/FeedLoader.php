<?php

namespace DStaroselskyi\RekrutacjaHRtec\Adapters;

use DStaroselskyi\RekrutacjaHRtec\Builders\Models\Feed\FromFeedIo\FeedBuilder;
use DStaroselskyi\RekrutacjaHRtec\Builders\Models\Feed\FromFeedIo\ItemBuilder;
use DStaroselskyi\RekrutacjaHRtec\Contracts\FeedLoader as FeedLoaderContract;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Feed;

/**
 * Adapter load`s feed using package debril/feed-io.
 */
class FeedLoader implements FeedLoaderContract
{
    /**
     * @var \FeedIo\FeedIo
     */
    protected $newsLoader;

    public function __construct()
    {
        $this->newsLoader = \FeedIo\Factory::create()->getFeedIo();
    }

    /**
     * @param string      $fromUrl
     * @param string|null $user
     * @param string|null $pass
     *
     * @return Feed
     */
    public function load(string $fromUrl, string $user = null, string $pass = null): Feed
    {
        $feed = $this->newsLoader->read($fromUrl);

        $newsBuilder = new FeedBuilder(new ItemBuilder());

        return $newsBuilder->build($feed);
    }
}
