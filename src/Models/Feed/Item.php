<?php

namespace DStaroselskyi\RekrutacjaHRtec\Models\Feed;

use Carbon\Carbon;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Item as ItemContract;

class Item implements ItemContract
{
    /** @var string */
    protected $title = '';
    /** @var string */
    protected $description = '';
    /** @var string */
    protected $link = '';
    /** @var Carbon */
    protected $pubDate;
    /** @var string */
    protected $creator = '';

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink(string $link): void
    {
        $this->link = $link;
    }

    /**
     * @return Carbon
     */
    public function getPubDate(): Carbon
    {
        return $this->pubDate;
    }

    /**
     * @param Carbon $pubDate
     */
    public function setPubDate(Carbon $pubDate): void
    {
        $this->pubDate = $pubDate;
    }

    /**
     * @return string
     */
    public function getCreator(): string
    {
        return $this->creator;
    }

    /**
     * @param string $creator
     */
    public function setCreator(string $creator): void
    {
        $this->creator = $creator;
    }
}
