<?php

namespace DStaroselskyi\RekrutacjaHRtec\Models\Feed;

use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Feed as FeedContract;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Item;
use Illuminate\Support\Collection;

class Feed implements FeedContract
{
    /**
     * @var Collection|Item[]
     */
    protected $items;

    public function __construct()
    {
        $this->items = new Collection();
    }

    /**
     * @param Item $item
     */
    public function pushItem(Item $item): void
    {
        $this->items->push($item);
    }

    /**
     * @return Collection|Item[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }
}
