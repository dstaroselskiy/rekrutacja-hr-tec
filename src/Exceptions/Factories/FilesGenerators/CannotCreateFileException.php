<?php

namespace DStaroselskyi\RekrutacjaHRtec\Exceptions\Factories\FilesGenerators;

class CannotCreateFileException extends \Exception
{
}
