<?php

namespace DStaroselskyi\RekrutacjaHRtec\Exceptions\Factories\FilesGenerators;

class FileExtensionIsNotCorrectException extends \Exception
{
}
