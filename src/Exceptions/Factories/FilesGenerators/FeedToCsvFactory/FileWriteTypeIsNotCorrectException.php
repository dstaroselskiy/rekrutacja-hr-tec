<?php

namespace DStaroselskyi\RekrutacjaHRtec\Exceptions\Factories\FilesGenerators\FeedToCsvFactory;

class FileWriteTypeIsNotCorrectException extends \Exception
{
}
