<?php

require __DIR__ . '/../vendor/autoload.php';

use DStaroselskyi\RekrutacjaHRtec\Commands\CsvExtendedFeedLoaderCommand;
use DStaroselskyi\RekrutacjaHRtec\Commands\CsvSimpleFeedLoaderCommand;
use Symfony\Component\Console\Application;

$application = new Application();

// ... register commands
// ...
$application->add(new CsvSimpleFeedLoaderCommand());
$application->add(new CsvExtendedFeedLoaderCommand());

$application->run();
