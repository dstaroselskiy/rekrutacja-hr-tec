<?php

namespace DStaroselskyi\RekrutacjaHRtec\Factories\FilesGenerators;

use DStaroselskyi\RekrutacjaHRtec\Contracts\Factories\FilesGenerators\FeedToCsvFactory as FeedToCsvFactoryContract;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Services\FilesGenerators\FeedCsv\FeedToCsv;
use DStaroselskyi\RekrutacjaHRtec\Exceptions\Factories\FilesGenerators\FeedToCsvFactory\FileWriteTypeIsNotCorrectException;
use DStaroselskyi\RekrutacjaHRtec\Helpers\FilesGenerators\FeedToCsvWriteTypes;
use DStaroselskyi\RekrutacjaHRtec\Services\FilesGenerators\FeedCsv\ColumnsStrategies\CreatorColumn;
use DStaroselskyi\RekrutacjaHRtec\Services\FilesGenerators\FeedCsv\ColumnsStrategies\DescriptionColumn;
use DStaroselskyi\RekrutacjaHRtec\Services\FilesGenerators\FeedCsv\ColumnsStrategies\LinkColumn;
use DStaroselskyi\RekrutacjaHRtec\Services\FilesGenerators\FeedCsv\ColumnsStrategies\PubDateColumn;
use DStaroselskyi\RekrutacjaHRtec\Services\FilesGenerators\FeedCsv\ColumnsStrategies\TitleColumn;
use DStaroselskyi\RekrutacjaHRtec\Services\FilesGenerators\FeedCsv\ExtendFeedToCsv;
use DStaroselskyi\RekrutacjaHRtec\Services\FilesGenerators\FeedCsv\FeedCsvStructure;
use DStaroselskyi\RekrutacjaHRtec\Services\FilesGenerators\FeedCsv\SaveFeedToCsv;

class FeedToCsvFactory implements FeedToCsvFactoryContract
{
    /**
     * @param string $type
     *
     * @throws \Exception
     *
     * @return FeedToCsv
     */
    public function make(string $type = FeedToCsvWriteTypes::SIMPLE): FeedToCsv
    {
        $columns = collect([
            new TitleColumn(),
            new DescriptionColumn(),
            new LinkColumn(),
            new PubDateColumn(),
            new CreatorColumn(),
        ]);

        $csvStructure = new FeedCsvStructure($columns);

        switch ($type) {
            case FeedToCsvWriteTypes::SIMPLE: return new SaveFeedToCsv($csvStructure);
            case FeedToCsvWriteTypes::EXTENDED: return new ExtendFeedToCsv($csvStructure);
        }

        throw new FileWriteTypeIsNotCorrectException("File write type ({$type}) is not correct");
    }
}
