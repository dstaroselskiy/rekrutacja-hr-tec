<?php

namespace DStaroselskyi\RekrutacjaHRtec\Services\FilesGenerators\FeedCsv;

use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Feed;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Item;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Services\FilesGenerators\FeedCsv\FeedToCsv as FeedToCsvContract;
use DStaroselskyi\RekrutacjaHRtec\Exceptions\Factories\FilesGenerators\CannotCreateFileException;
use DStaroselskyi\RekrutacjaHRtec\Exceptions\Factories\FilesGenerators\FileExtensionIsNotCorrectException;

abstract class FeedToCsv implements FeedToCsvContract
{
    /** @var FeedCsvStructure */
    protected $feedCsvStructure;

    /** @var resource */
    protected $fileResource;

    /**
     * @param FeedCsvStructure $feedCsvStructure
     */
    public function __construct(FeedCsvStructure $feedCsvStructure)
    {
        $this->feedCsvStructure = $feedCsvStructure;
    }

    /**
     * @param string $saveFeedToFile
     *
     * @return bool
     */
    protected function checkFileExtension(string $saveFeedToFile): bool
    {
        $extension = pathinfo($saveFeedToFile, PATHINFO_EXTENSION);

        return 'csv' == $extension;
    }

    /**
     * @param string $saveFeedToFile
     *
     * @throws CannotCreateFileException
     * @throws FileExtensionIsNotCorrectException
     */
    protected function openFile(string $saveFeedToFile): void
    {
        if ($this->checkFileExtension($saveFeedToFile)) {
            if ($resource = fopen($saveFeedToFile, $this->getFileWriteMode())) {
                $this->fileResource = $resource;
            } else {
                throw new CannotCreateFileException("Cannot creat file {$saveFeedToFile}");
            }
        } else {
            throw new FileExtensionIsNotCorrectException("File extension must bee .csv ({$saveFeedToFile})");
        }
    }

    /**
     * @return bool
     */
    protected function closeFile(): bool
    {
        return fclose($this->fileResource);
    }

    /**
     * @param string $content
     *
     * @return false|int
     */
    protected function writeToFile(string $content)
    {
        return fwrite($this->fileResource, $content . PHP_EOL);
    }

    /**
     * @param Feed $feed
     */
    protected function writeFeedRows(Feed $feed): void
    {
        $feed->getItems()
            ->each(function (Item $item): void {
                $this->writeToFile($this->feedCsvStructure->getRow($item));
            });
    }

    /**
     * @return string
     */
    abstract protected function getFileWriteMode(): string;
}
