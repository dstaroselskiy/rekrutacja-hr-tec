<?php

namespace DStaroselskyi\RekrutacjaHRtec\Services\FilesGenerators\FeedCsv\ColumnsStrategies;

use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Item;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Services\FilesGenerators\FeedCsv\ColumnsStrategies\TitleColumn as TitleColumnContract;

class TitleColumn implements TitleColumnContract
{
    /**
     * @return string
     */
    public function getTitle(): string
    {
        return 'title';
    }

    /**
     * @param Item $object
     *
     * @return string
     */
    public function getValue(object $object): string
    {
        return htmlentities($object->getTitle());
    }
}
