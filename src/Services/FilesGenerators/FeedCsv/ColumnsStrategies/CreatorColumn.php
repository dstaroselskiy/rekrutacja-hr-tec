<?php

namespace DStaroselskyi\RekrutacjaHRtec\Services\FilesGenerators\FeedCsv\ColumnsStrategies;

use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Item;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Services\FilesGenerators\FeedCsv\ColumnsStrategies\TitleColumn as TitleColumnContract;

class CreatorColumn implements TitleColumnContract
{
    /**
     * @return string
     */
    public function getTitle(): string
    {
        return 'creator';
    }

    /**
     * @param Item $object
     *
     * @return string
     */
    public function getValue(object $object): string
    {
        return htmlentities($object->getCreator());
    }
}
