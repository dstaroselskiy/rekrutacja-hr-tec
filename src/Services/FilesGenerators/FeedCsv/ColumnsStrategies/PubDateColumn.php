<?php

namespace DStaroselskyi\RekrutacjaHRtec\Services\FilesGenerators\FeedCsv\ColumnsStrategies;

use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Item;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Services\FilesGenerators\FeedCsv\ColumnsStrategies\TitleColumn as TitleColumnContract;

class PubDateColumn implements TitleColumnContract
{
    /**
     * @return string
     */
    public function getTitle(): string
    {
        return 'pubDate';
    }

    /**
     * @param Item $object
     *
     * @return string
     */
    public function getValue(object $object): string
    {
        return $object->getPubDate()
            ->locale('pl_PL')
            ->isoFormat('DD MMMM YYYY hh:mm:ss');
    }
}
