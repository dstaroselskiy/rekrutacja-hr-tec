<?php

namespace DStaroselskyi\RekrutacjaHRtec\Services\FilesGenerators\FeedCsv;

use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Feed;

class ExtendFeedToCsv extends FeedToCsv
{
    /**
     * @return string
     */
    protected function getFileWriteMode(): string
    {
        return 'a';
    }

    /**
     * @param Feed   $feed
     * @param string $saveFeedToFile
     *
     * @throws \DStaroselskyi\RekrutacjaHRtec\Exceptions\Factories\FilesGenerators\CannotCreateFileException
     * @throws \DStaroselskyi\RekrutacjaHRtec\Exceptions\Factories\FilesGenerators\FileExtensionIsNotCorrectException
     *
     * @return bool
     */
    public function save(Feed $feed, string $saveFeedToFile): bool
    {
        if (! file_exists($saveFeedToFile)) {
            $this->openFile($saveFeedToFile);
            $this->writeToFile($this->feedCsvStructure->getTitleRow());
        } else {
            $this->openFile($saveFeedToFile);
        }
        $this->writeFeedRows($feed);

        return $this->closeFile();
    }
}
