<?php

namespace DStaroselskyi\RekrutacjaHRtec\Services\FilesGenerators\FeedCsv;

use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Item;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Services\FilesGenerators\ColumnsStrategy;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Services\FilesGenerators\CsvStructureGenerator;
use Illuminate\Support\Collection;

class FeedCsvStructure implements CsvStructureGenerator
{
    /** @var string */
    protected $csvSeparator = ',';
    /** @var string */
    protected $enclosure = '"';
    /** @var Collection */
    protected $columns;

    /**
     * @param Collection $columns
     */
    public function __construct(Collection $columns)
    {
        $this->columns = $columns;
    }

    /**
     * @param array $row
     *
     * @return string
     */
    protected function prepareRow(array $row): string
    {
        return $this->enclosure
            . implode($this->enclosure . $this->csvSeparator . $this->enclosure, $row)
            . $this->enclosure;
    }

    /**
     * @return array
     */
    protected function getColumnsNames(): array
    {
        return $this->columns->map(function (ColumnsStrategy $columnsStrategy): string {
            return $columnsStrategy->getTitle();
        })->toArray();
    }

    /**
     * @param Item $item
     *
     * @return array
     */
    protected function getRowFrom(Item $item): array
    {
        return $this->columns
            ->map(function (ColumnsStrategy $columnsStrategy) use ($item): string {
                return $columnsStrategy->getValue($item);
            })->toArray();
    }

    /**
     * @return string
     */
    public function getTitleRow(): string
    {
        return $this->prepareRow($this->getColumnsNames());
    }

    /**
     * @param Item|object $object
     *
     * @return string
     */
    public function getRow(object $object): string
    {
        return $this->prepareRow($this->getRowFrom($object));
    }
}
