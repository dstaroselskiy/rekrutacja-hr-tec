<?php

namespace DStaroselskyi\RekrutacjaHRtec\Services\FilesGenerators\FeedCsv;

use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Feed;

class SaveFeedToCsv extends FeedToCsv
{
    /**
     * @return string
     */
    protected function getFileWriteMode(): string
    {
        return 'w';
    }

    /**
     * @param Feed   $feed
     * @param string $saveFeedToFile
     *
     * @throws \DStaroselskyi\RekrutacjaHRtec\Exceptions\Factories\FilesGenerators\CannotCreateFileException
     * @throws \DStaroselskyi\RekrutacjaHRtec\Exceptions\Factories\FilesGenerators\FileExtensionIsNotCorrectException
     *
     * @return bool
     */
    public function save(Feed $feed, string $saveFeedToFile): bool
    {
        $this->openFile($saveFeedToFile);
        $this->writeToFile($this->feedCsvStructure->getTitleRow());
        $this->writeFeedRows($feed);

        return $this->closeFile();
    }
}
