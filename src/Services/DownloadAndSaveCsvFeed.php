<?php

namespace DStaroselskyi\RekrutacjaHRtec\Services;

use DStaroselskyi\RekrutacjaHRtec\Contracts\FeedLoader;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Services\FilesGenerators\FeedCsv\FeedToCsv;

class DownloadAndSaveCsvFeed
{
    /** @var FeedLoader */
    protected $feedLoader;
    /** @var FeedToCsv */
    protected $feedToCsv;

    /**
     * @param FeedLoader $feedLoader
     * @param FeedToCsv  $feedToCsv
     */
    public function __construct(FeedLoader $feedLoader, FeedToCsv $feedToCsv)
    {
        $this->feedLoader = $feedLoader;
        $this->feedToCsv = $feedToCsv;
    }

    /**
     * @param string $uploadFeedFromUrl
     * @param string $saveFeedToFile
     *
     * @return bool
     */
    public function run(string $uploadFeedFromUrl, string $saveFeedToFile): bool
    {
        $feed = $this->feedLoader->load($uploadFeedFromUrl);

        return $this->feedToCsv->save($feed, $saveFeedToFile);
    }
}
