<?php

namespace DStaroselskyi\RekrutacjaHRtec\Helpers\FilesGenerators;

abstract class FeedToCsvWriteTypes
{
    public const SIMPLE = 'simple';
    public const EXTENDED = 'extended';
}
