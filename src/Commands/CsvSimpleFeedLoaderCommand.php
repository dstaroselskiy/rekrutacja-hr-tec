<?php

namespace DStaroselskyi\RekrutacjaHRtec\Commands;

use DStaroselskyi\RekrutacjaHRtec\Helpers\FilesGenerators\FeedToCsvWriteTypes;
use Symfony\Component\Console\Command\Command;

class CsvSimpleFeedLoaderCommand extends AbstractFeedLoaderCommand
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'csv:simple';

    protected function configure(): void
    {
        parent::configure();

        $this
            ->setDescription('Download news from RSS / Atom and save it to csv');
    }

    /**
     * @return string
     */
    protected function getCsvWriteType(): string
    {
        return FeedToCsvWriteTypes::SIMPLE;
    }
}
