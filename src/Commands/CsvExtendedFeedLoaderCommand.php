<?php

namespace DStaroselskyi\RekrutacjaHRtec\Commands;

use DStaroselskyi\RekrutacjaHRtec\Helpers\FilesGenerators\FeedToCsvWriteTypes;
use Symfony\Component\Console\Command\Command;

class CsvExtendedFeedLoaderCommand extends AbstractFeedLoaderCommand
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'csv:extended';

    protected function configure(): void
    {
        parent::configure();

        $this
            ->setDescription('Download articles from RSS / Atom and append it to csv');
    }

    /**
     * @return string
     */
    protected function getCsvWriteType(): string
    {
        return FeedToCsvWriteTypes::EXTENDED;
    }
}
