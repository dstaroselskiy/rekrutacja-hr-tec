<?php

namespace DStaroselskyi\RekrutacjaHRtec\Commands;

use DStaroselskyi\RekrutacjaHRtec\Adapters\FeedLoader;
use DStaroselskyi\RekrutacjaHRtec\Factories\FilesGenerators\FeedToCsvFactory;
use DStaroselskyi\RekrutacjaHRtec\Services\DownloadAndSaveCsvFeed;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class AbstractFeedLoaderCommand extends Command
{
    protected function configure(): void
    {
        $this
            ->addArgument('URL', InputArgument::REQUIRED, 'Url on news RSS / Atom')
            ->addArgument('PATH', InputArgument::REQUIRED, 'Path to save news');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws Exception
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $uploadFeedFromUrl = $input->getArgument('URL');
        $saveFeedToFile = $input->getArgument('PATH');
        try {
            if ((new DownloadAndSaveCsvFeed(
                new FeedLoader(),
                (new FeedToCsvFactory())->make($this->getCsvWriteType())
            ))->run($uploadFeedFromUrl, $saveFeedToFile)) {
                $output->writeln('Success: Feed is written into file ' . $saveFeedToFile);

                return $this::SUCCESS;
            }
            $output->writeln('Failure: Feed is not written into file ' . $saveFeedToFile);

            return $this::FAILURE;
        } catch (\Exception $error) {
            $output->writeln('Error: ' . $error->getMessage());

            return $this::INVALID;
        }
    }

    /**
     * @return string
     */
    abstract protected function getCsvWriteType(): string;
}
