<?php

namespace DStaroselskyi\RekrutacjaHRtec\Builders;

use DStaroselskyi\RekrutacjaHRtec\Contracts\Builders\Builder;

abstract class AbstractBuilder implements Builder
{
    /** @var object */
    protected $model;

    public function __construct()
    {
        $this->reset();
    }

    /**
     * Create clear model.
     *
     * @return Builder
     */
    public function reset(): Builder
    {
        $className = $this->getClassForBuilder();
        $this->model = new $className();

        return $this;
    }

    /**
     * Return model and reset it state.
     *
     * @return mixed
     */
    public function getModel()
    {
        $model = clone $this->model;
        $this->reset();

        return $model;
    }

    /**
     * Factory method - return class name for builder.
     *
     * @return string
     */
    abstract protected function getClassForBuilder(): string;
}
