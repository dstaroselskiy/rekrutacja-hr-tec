<?php

namespace DStaroselskyi\RekrutacjaHRtec\Builders\Models\Feed\FromSimpleXMLElement;

use DStaroselskyi\RekrutacjaHRtec\Builders\Models\Feed\FeedBuilder as MainFeedBuilder;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Builders\Models\Feed\FromSimpleXMLElement\FeedBuilder as FeedBuilderContract;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Builders\Models\Feed\FromSimpleXMLElement\ItemBuilder as ItemBuilderContract;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Feed;

class FeedBuilder extends MainFeedBuilder implements FeedBuilderContract
{
    /** @var ItemBuilderContract */
    protected $itemBuilder;

    /**
     * @param ItemBuilderContract $itemBuilder
     */
    public function __construct(ItemBuilderContract $itemBuilder)
    {
        $this->itemBuilder = $itemBuilder;
        parent::__construct();
    }

    /**
     * @param \SimpleXMLElement $from
     *
     * @return Feed
     */
    public function build(\SimpleXMLElement $from): Feed
    {
        // TODO: late
        return $this->getModel();
    }
}
