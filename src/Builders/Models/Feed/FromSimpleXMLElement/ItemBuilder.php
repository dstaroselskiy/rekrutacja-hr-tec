<?php

namespace DStaroselskyi\RekrutacjaHRtec\Builders\Models\Feed\FromSimpleXMLElement;

use Carbon\Carbon;
use DStaroselskyi\RekrutacjaHRtec\Builders\Models\Feed\ItemBuilder as MainItemBuilder;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Builders\Models\Feed\FromSimpleXMLElement\ItemBuilder as ItemBuilderFromSimpleXMLElementContract;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Builders\Models\Feed\ItemBuilder as ItemBuilderContract;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Item;

class ItemBuilder extends MainItemBuilder implements ItemBuilderFromSimpleXMLElementContract
{
    /**
     * @param string $description
     *
     * @return ItemBuilderContract
     */
    public function setDescription(string $description): ItemBuilderContract
    {
        return parent::setDescription(strip_tags($description));
    }

    /**
     * @param \SimpleXMLElement $from
     *
     * @return Item
     */
    public function build(\SimpleXMLElement $from): Item
    {
        return $this->setCreator($from->{'dc:creator'} ?? '')
            ->setTitle($from->{'title'})
            ->setDescription($from->{'description'})
            ->setLink($from->{'link'})
            ->setPubDate(Carbon::createFromTimestamp($from->{'timestamp'}))
            ->getModel();
    }
}
