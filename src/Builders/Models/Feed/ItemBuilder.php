<?php

namespace DStaroselskyi\RekrutacjaHRtec\Builders\Models\Feed;

use Carbon\Carbon;
use DStaroselskyi\RekrutacjaHRtec\Builders\AbstractBuilder;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Builders\Models\Feed\ItemBuilder as ItemBuilderContract;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Item;
use DStaroselskyi\RekrutacjaHRtec\Models\Feed\Item as ItemModel;

/**
 * @property Item $model
 */
class ItemBuilder extends AbstractBuilder implements ItemBuilderContract
{
    /**
     * @return string
     */
    protected function getClassForBuilder(): string
    {
        return ItemModel::class;
    }

    /**
     * @param string $title
     *
     * @return ItemBuilderContract
     */
    public function setTitle(string $title): ItemBuilderContract
    {
        $this->model->setTitle($title);

        return $this;
    }

    /**
     * @param string $description
     *
     * @return ItemBuilderContract
     */
    public function setDescription(string $description): ItemBuilderContract
    {
        $this->model->setDescription($description);

        return $this;
    }

    /**
     * @param string $link
     *
     * @return ItemBuilderContract
     */
    public function setLink(string $link): ItemBuilderContract
    {
        $this->model->setLink($link);

        return $this;
    }

    /**
     * @param Carbon $pubDate
     *
     * @return ItemBuilderContract
     */
    public function setPubDate(Carbon $pubDate): ItemBuilderContract
    {
        $this->model->setPubDate($pubDate);

        return $this;
    }

    /**
     * @param string $creator
     *
     * @return ItemBuilderContract
     */
    public function setCreator(string $creator): ItemBuilderContract
    {
        $this->model->setCreator($creator);

        return $this;
    }
}
