<?php

namespace DStaroselskyi\RekrutacjaHRtec\Builders\Models\Feed;

use DStaroselskyi\RekrutacjaHRtec\Builders\AbstractBuilder;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Builders\Models\Feed\FeedBuilder as FeedBuilderContract;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Feed;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Item;
use DStaroselskyi\RekrutacjaHRtec\Models\Feed\Feed as FeedModel;

/**
 * @property Feed $model
 */
class FeedBuilder extends AbstractBuilder implements FeedBuilderContract
{
    /**
     * @return string
     */
    protected function getClassForBuilder(): string
    {
        return FeedModel::class;
    }

    /**
     * @param Item $item
     *
     * @return FeedBuilderContract
     */
    public function pushItem(Item $item): FeedBuilderContract
    {
        $this->model->pushItem($item);

        return $this;
    }
}
