<?php

namespace DStaroselskyi\RekrutacjaHRtec\Builders\Models\Feed\FromFeedIo;

use Carbon\Carbon;
use DStaroselskyi\RekrutacjaHRtec\Builders\Models\Feed\ItemBuilder as MainItemBuilder;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Builders\Models\Feed\FromFeedIo\ItemBuilder as ItemBuilderFomFromFeedIoContract;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Builders\Models\Feed\ItemBuilder as ItemBuilderContract;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Item;
use FeedIo\Feed\ItemInterface;

/**
 * Builder create platform`s FeedItem model from models by package debril/feed-io.
 */
class ItemBuilder extends MainItemBuilder implements ItemBuilderFomFromFeedIoContract
{
    /**
     * @param string $description
     *
     * @return ItemBuilderContract
     */
    public function setDescription(string $description): ItemBuilderContract
    {
        $description = preg_replace(
            [
                '/<script(.*?)>(.*?)<\/script>/is',
                '/<style(.*?)>(.*?)<\/style>/is',
            ],
            '',
            $description
        );

        return parent::setDescription(strip_tags($description));
    }

    /**
     * @param ItemInterface $from
     *
     * @return Item
     */
    public function build(ItemInterface $from): Item
    {
        return $this
            ->setTitle($from->getTitle())
            ->setDescription($from->getContent())
            ->setCreator($from->getAuthor() ? ($from->getAuthor()->getName() ?: '') : '')
            ->setLink($from->getLink())
            ->setPubDate(Carbon::make($from->getLastModified()))
            ->getModel();
    }
}
