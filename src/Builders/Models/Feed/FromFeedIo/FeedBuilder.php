<?php

namespace DStaroselskyi\RekrutacjaHRtec\Builders\Models\Feed\FromFeedIo;

use DStaroselskyi\RekrutacjaHRtec\Builders\Models\Feed\FeedBuilder as MainFeedBuilder;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Builders\Models\Feed\FromFeedIo\FeedBuilder as FeedBuilderContract;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Builders\Models\Feed\FromFeedIo\ItemBuilder as ItemBuilderContract;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Feed;

/**
 * Builder create platform`s Feed model from models by package debril/feed-io.
 */
class FeedBuilder extends MainFeedBuilder implements FeedBuilderContract
{
    /** @var ItemBuilderContract */
    protected $itemBuilder;

    /**
     * @param ItemBuilderContract $itemBuilder
     */
    public function __construct(ItemBuilderContract $itemBuilder)
    {
        $this->itemBuilder = $itemBuilder;
        parent::__construct();
    }

    /**
     * @param \FeedIo\Reader\Result $feed
     *
     * @return Feed
     */
    public function build(\FeedIo\Reader\Result $feed): Feed
    {
        foreach ($feed->getFeed() as $feedItem) {
            $item = $this->itemBuilder->build($feedItem);
            $this->pushItem($item);
        }

        return $this->getModel();
    }
}
