<?php

namespace DStaroselskyi\RekrutacjaHRtec\Contracts\Builders\Models\Feed\FromFeedIo;

use DStaroselskyi\RekrutacjaHRtec\Contracts\Builders\Models\Feed\FeedBuilder as MainFeedBuilder;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Feed;

/**
 * Builder create platform`s Feed model from models by package debril/feed-io.
 */
interface FeedBuilder extends MainFeedBuilder
{
    /**
     * @param \FeedIo\Reader\Result $feed
     *
     * @return Feed
     */
    public function build(\FeedIo\Reader\Result $feed): Feed;
}
