<?php

namespace DStaroselskyi\RekrutacjaHRtec\Contracts\Builders\Models\Feed\FromFeedIo;

use DStaroselskyi\RekrutacjaHRtec\Contracts\Builders\Models\Feed\ItemBuilder as MainItemBuilder;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Item;
use FeedIo\Feed\ItemInterface;

/**
 * Builder create platform`s FeedItem model from models by package debril/feed-io.
 */
interface ItemBuilder extends MainItemBuilder
{
    /**
     * @param ItemInterface $from
     *
     * @return Item
     */
    public function build(ItemInterface $from): Item;
}
