<?php

namespace DStaroselskyi\RekrutacjaHRtec\Contracts\Builders\Models\Feed\FromSimpleXMLElement;

use DStaroselskyi\RekrutacjaHRtec\Contracts\Builders\Models\Feed\FeedBuilder as MainFeedBuilder;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Feed;

interface FeedBuilder extends MainFeedBuilder
{
    /**
     * @param \SimpleXMLElement $from
     *
     * @return Feed
     */
    public function build(\SimpleXMLElement $from): Feed;
}
