<?php

namespace DStaroselskyi\RekrutacjaHRtec\Contracts\Builders\Models\Feed\FromSimpleXMLElement;

use DStaroselskyi\RekrutacjaHRtec\Contracts\Builders\Models\Feed\ItemBuilder as MainItemBuilder;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Item;

interface ItemBuilder extends MainItemBuilder
{
    /**
     * @param \SimpleXMLElement $from
     *
     * @return Item
     */
    public function build(\SimpleXMLElement $from): Item;
}
