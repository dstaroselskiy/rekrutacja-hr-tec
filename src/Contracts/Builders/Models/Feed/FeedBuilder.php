<?php

namespace DStaroselskyi\RekrutacjaHRtec\Contracts\Builders\Models\Feed;

use DStaroselskyi\RekrutacjaHRtec\Contracts\Builders\Builder;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Item;

interface FeedBuilder extends Builder
{
    /**
     * @param Item $item
     *
     * @return $this
     */
    public function pushItem(Item $item): self;
}
