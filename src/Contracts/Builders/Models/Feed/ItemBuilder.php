<?php

namespace DStaroselskyi\RekrutacjaHRtec\Contracts\Builders\Models\Feed;

use Carbon\Carbon;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Builders\Builder;

interface ItemBuilder extends Builder
{
    /**
     * @param string $title
     */
    public function setTitle(string $title): self;

    /**
     * @param string $description
     */
    public function setDescription(string $description): self;

    /**
     * @param string $link
     */
    public function setLink(string $link): self;

    /**
     * @param Carbon $pubDate
     */
    public function setPubDate(Carbon $pubDate): self;

    /**
     * @param string $creator
     */
    public function setCreator(string $creator): self;
}
