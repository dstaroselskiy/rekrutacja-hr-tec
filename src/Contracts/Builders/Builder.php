<?php

namespace DStaroselskyi\RekrutacjaHRtec\Contracts\Builders;

interface Builder
{
    /**
     * Reset object state.
     *
     * @return $this
     */
    public function reset(): self;

    /**
     * Return object and reset state in builder.
     *
     * @return mixed
     */
    public function getModel();
}
