<?php

namespace DStaroselskyi\RekrutacjaHRtec\Contracts\Services\FilesGenerators\FeedCsv\ColumnsStrategies;

use DStaroselskyi\RekrutacjaHRtec\Contracts\Services\FilesGenerators\ColumnsStrategy;

interface TitleColumn extends ColumnsStrategy
{
}
