<?php

namespace DStaroselskyi\RekrutacjaHRtec\Contracts\Services\FilesGenerators\FeedCsv;

use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Feed;

interface FeedToCsv
{
    /**
     * @param Feed   $feed
     * @param string $saveFeedToFile
     *
     * @return bool
     */
    public function save(Feed $feed, string $saveFeedToFile): bool;
}
