<?php

namespace DStaroselskyi\RekrutacjaHRtec\Contracts\Services\FilesGenerators\FilesContents;

interface FilesHeaderContent
{
    /**
     * @return string
     */
    public function getTitleRow(): string;
}
