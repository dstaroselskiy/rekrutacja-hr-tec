<?php

namespace DStaroselskyi\RekrutacjaHRtec\Contracts\Services\FilesGenerators\FilesContents;

interface FilesRowContent
{
    /**
     * @param object $object
     *
     * @return string
     */
    public function getRow(object $object): string;
}
