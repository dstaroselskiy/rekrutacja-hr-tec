<?php

namespace DStaroselskyi\RekrutacjaHRtec\Contracts\Services\FilesGenerators;

use DStaroselskyi\RekrutacjaHRtec\Contracts\Services\FilesGenerators\FilesContents\FilesHeaderContent;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Services\FilesGenerators\FilesContents\FilesRowContent;

interface CsvStructureGenerator extends FilesHeaderContent, FilesRowContent
{
}
