<?php

namespace DStaroselskyi\RekrutacjaHRtec\Contracts\Services\FilesGenerators;

interface ColumnsStrategy
{
    /**
     * @return string
     */
    public function getTitle(): string;

    /**
     * @param object $object
     *
     * @return string
     */
    public function getValue(object $object): string;
}
