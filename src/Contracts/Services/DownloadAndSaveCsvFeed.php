<?php

namespace DStaroselskyi\RekrutacjaHRtec\Contracts\Services;

interface DownloadAndSaveCsvFeed
{
    /**
     * @param string $uploadFeedFromUrl
     * @param string $saveFeedToFile
     *
     * @return bool
     */
    public function run(string $uploadFeedFromUrl, string $saveFeedToFile): bool;
}
