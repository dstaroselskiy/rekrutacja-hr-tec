<?php

namespace DStaroselskyi\RekrutacjaHRtec\Contracts;

use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Feed;

interface FeedLoader
{
    /**
     * @param string      $fromUrl
     * @param string|null $user
     * @param string|null $pass
     *
     * @return Feed
     */
    public function load(string $fromUrl, string $user = null, string $pass = null): Feed;
}
