<?php

namespace DStaroselskyi\RekrutacjaHRtec\Contracts\Factories\FilesGenerators;

use DStaroselskyi\RekrutacjaHRtec\Contracts\Services\FilesGenerators\FeedCsv\FeedToCsv;
use DStaroselskyi\RekrutacjaHRtec\Helpers\FilesGenerators\FeedToCsvWriteTypes;

interface FeedToCsvFactory
{
    /**
     * @param string $type
     *
     * @return FeedToCsv
     */
    public function make(string $type = FeedToCsvWriteTypes::SIMPLE): FeedToCsv;
}
