<?php

namespace DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed;

use Carbon\Carbon;

interface Item
{
    /**
     * @return string
     */
    public function getTitle(): string;

    /**
     * @param string $title
     */
    public function setTitle(string $title): void;

    /**
     * @return string
     */
    public function getDescription(): string;

    /**
     * @param string $description
     */
    public function setDescription(string $description): void;

    /**
     * @return string
     */
    public function getLink(): string;

    /**
     * @param string $link
     */
    public function setLink(string $link): void;

    /**
     * @return Carbon
     */
    public function getPubDate(): Carbon;

    /**
     * @param Carbon $pubDate
     */
    public function setPubDate(Carbon $pubDate): void;

    /**
     * @return string
     */
    public function getCreator(): string;

    /**
     * @param string $creator
     */
    public function setCreator(string $creator): void;
}
