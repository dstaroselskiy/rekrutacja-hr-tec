<?php

namespace DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed;

use Illuminate\Support\Collection;

interface Feed
{
    /**
     * @param Item $item
     */
    public function pushItem(Item $item): void;

    /**
     * @return Collection|Item[]
     */
    public function getItems(): Collection;
}
