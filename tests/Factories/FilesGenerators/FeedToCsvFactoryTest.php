<?php

namespace DStaroselskyi\RekrutacjaHRtec\Tests\Factories\FilesGenerators;

use DStaroselskyi\RekrutacjaHRtec\Factories\FilesGenerators\FeedToCsvFactory;
use DStaroselskyi\RekrutacjaHRtec\Helpers\FilesGenerators\FeedToCsvWriteTypes;
use DStaroselskyi\RekrutacjaHRtec\Services\FilesGenerators\FeedCsv\ExtendFeedToCsv;
use DStaroselskyi\RekrutacjaHRtec\Services\FilesGenerators\FeedCsv\SaveFeedToCsv;
use PHPUnit\Framework\TestCase;

class FeedToCsvFactoryTest extends TestCase
{
    public function testFeedToCsvFactory(): void
    {
        $factory = new FeedToCsvFactory();

        $feedToCsv = $factory->make(FeedToCsvWriteTypes::SIMPLE);
        $this->assertEquals(get_class($feedToCsv), SaveFeedToCsv::class, 'FeedToCsvFactory not return correct object for FeedToCsvWriteTypes::SIMPLE');

        $feedToCsv = $factory->make(FeedToCsvWriteTypes::EXTENDED);
        $this->assertEquals(get_class($feedToCsv), ExtendFeedToCsv::class, 'FeedToCsvFactory not return correct object for FeedToCsvWriteTypes::EXTENDED');
    }
}
