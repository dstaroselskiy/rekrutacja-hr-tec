<?php

namespace DStaroselskyi\RekrutacjaHRtec\Tests\Builders\Models;

use Carbon\Carbon;
use DStaroselskyi\RekrutacjaHRtec\Builders\Models\Feed;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Item;
use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;

class FeedBuilderTest extends TestCase
{
    /**
     * Test that true does in fact equal true.
     */
    public function testMainFeedBuilder(): void
    {
        $item = new \DStaroselskyi\RekrutacjaHRtec\Models\Feed\Item();
        $feedFeedBuilder = new Feed\FeedBuilder();

        /** @var \DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Feed $feed */
        $feed = $feedFeedBuilder
            ->pushItem($item)
            ->getModel();

        $this->assertTrue(is_a($feed, \DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Feed::class));
        $this->assertEquals($feed->getItems()->count(), 1);
        $this->assertSame($feed->getItems()->first(), $item);

        /** @var \DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Feed $secondFeed */
        $secondFeed = $feedFeedBuilder->getModel();

        $this->assertNotSame($feed, $secondFeed, 'Feed builder is not reset model.');
    }

    /**
     * Test that true does in fact equal true.
     */
    public function testFeedItemBuilderFromFeedIo(): void
    {
        $feedIoFeedItemAuthorMock_1 = $this->createMock(\FeedIo\Feed\Item\Author::class);
        $feedIoFeedItemAuthorMock_1
            ->method('getName')
            ->willReturn('FeedIo\Feed\Item\Author-1');

        $feedIoFeedItemMock_1 = $this->createMock(\FeedIo\Feed\Item::class);
        $feedIoFeedItemMock_1
            ->method('getTitle')
            ->willReturn('FeedIo\Feed\Item\Title-1');

        $feedIoFeedItemMock_1
            ->method('getContent')
            ->willReturn('<p>FeedIo\Feed\Item\Content-1</p>');

        $feedIoFeedItemMock_1
            ->method('getLink')
            ->willReturn('FeedIo\Feed\Item\Link-1');
        $pubDate_1 = Carbon::now();

        $feedIoFeedItemMock_1
            ->method('getLastModified')
            ->willReturn($pubDate_1);

        $feedIoFeedItemMock_1
            ->method('getAuthor')
            ->willReturn($feedIoFeedItemAuthorMock_1);

        $feedIoFeedItemAuthorMock_2 = $this->createMock(\FeedIo\Feed\Item\Author::class);
        $feedIoFeedItemAuthorMock_2
            ->method('getName')
            ->willReturn('FeedIo\Feed\Item\Author-2');

        $feedIoFeedItemMock_2 = $this->createMock(\FeedIo\Feed\Item::class);
        $feedIoFeedItemMock_2
            ->method('getTitle')
            ->willReturn('FeedIo\Feed\Item\Title-2');

        $feedIoFeedItemMock_2
            ->method('getContent')
            ->willReturn('<p>FeedIo\Feed\Item\Content-2</p>');

        $feedIoFeedItemMock_2
            ->method('getLink')
            ->willReturn('FeedIo\Feed\Item\Link-2');
        $pubDate_2 = Carbon::now()->subDay();

        $feedIoFeedItemMock_2
            ->method('getLastModified')
            ->willReturn($pubDate_2);

        $feedIoFeedItemMock_2
            ->method('getAuthor')
            ->willReturn($feedIoFeedItemAuthorMock_2);

        $feedIoFeedMock = new \FeedIo\Feed();
        $feedIoFeedMock
            ->add($feedIoFeedItemMock_1)
            ->add($feedIoFeedItemMock_2);

        $feedIoFeedResultMock = $this->createMock(\FeedIo\Reader\Result::class);
        $feedIoFeedResultMock
            ->method('getFeed')
            ->willReturn($feedIoFeedMock);

        $feedBuilder = new Feed\FromFeedIo\FeedBuilder(new Feed\FromFeedIo\ItemBuilder());

        /** @var \DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Feed $feed */
        $feed = $feedBuilder->build($feedIoFeedResultMock);

        $this->assertTrue(is_a($feed, \DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Feed::class));

        $this->assertTrue(is_a($feed->getItems(), Collection::class));

        $this->assertEquals($feed->getItems()->count(), 2);

        /** @var Item $item1 */
        $item1 = $feed->getItems()->offsetGet(0);
        $this->assertEquals($item1->getCreator(), 'FeedIo\Feed\Item\Author-1');
        $this->assertEquals($item1->getLink(), 'FeedIo\Feed\Item\Link-1');
        $this->assertEquals($item1->getDescription(), 'FeedIo\Feed\Item\Content-1');
        $this->assertEquals($item1->getTitle(), 'FeedIo\Feed\Item\Title-1');
        $this->assertTrue(is_a($item1->getPubDate(), Carbon::class));
        $this->assertEquals($item1->getPubDate()->getTimestamp(), $pubDate_1->getTimestamp());
        /** @var Item $item2 */
        $item2 = $feed->getItems()->offsetGet(1);
        $this->assertEquals($item2->getCreator(), 'FeedIo\Feed\Item\Author-2');
        $this->assertEquals($item2->getLink(), 'FeedIo\Feed\Item\Link-2');
        $this->assertEquals($item2->getDescription(), 'FeedIo\Feed\Item\Content-2');
        $this->assertEquals($item2->getTitle(), 'FeedIo\Feed\Item\Title-2');
        $this->assertTrue(is_a($item2->getPubDate(), Carbon::class));
        $this->assertEquals($item2->getPubDate()->getTimestamp(), $pubDate_2->getTimestamp());

        /** @var \DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Feed $secondFeed */
        $secondFeed = $feedBuilder->build($feedIoFeedResultMock);

        $this->assertNotSame($feed, $secondFeed, 'Feed builder is not reset model.');
    }
}
