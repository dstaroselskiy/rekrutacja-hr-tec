<?php

namespace DStaroselskyi\RekrutacjaHRtec\Tests\Builders\Models;

use Carbon\Carbon;
use DStaroselskyi\RekrutacjaHRtec\Builders\Models\Feed;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Item;
use PHPUnit\Framework\TestCase;

class FeedItemBuilderTest extends TestCase
{
    /**
     * Test that true does in fact equal true.
     */
    public function testMainFeedItemBuilder(): void
    {
        $feedItemBuilder = new Feed\ItemBuilder();

        $pubDate = Carbon::now();
        /** @var Item $feedItem */
        $feedItem = $feedItemBuilder
            ->setLink('https://tets.com')
            ->setDescription('description')
            ->setTitle('Title')
            ->setCreator('Creator')
            ->setPubDate($pubDate)
            ->getModel();

        $this->assertTrue(is_a($feedItem, Item::class));

        $this->assertEquals($feedItem->getLink(), 'https://tets.com');
        $this->assertEquals($feedItem->getCreator(), 'Creator');
        $this->assertEquals($feedItem->getDescription(), 'description');
        $this->assertEquals($feedItem->getTitle(), 'Title');
        $this->assertTrue(is_a($feedItem->getPubDate(), Carbon::class));
        $this->assertEquals($feedItem->getPubDate(), $pubDate);
        $this->assertEquals($feedItem->getPubDate()->getTimestamp(), $pubDate->getTimestamp());

        /** @var Item $secondFeedItem */
        $secondFeedItem = $feedItemBuilder->getModel();

        $this->assertNotSame($feedItem, $secondFeedItem, 'Feed item builder is not reset model.');
    }

    /**
     * Test that true does in fact equal true.
     */
    public function testFeedItemBuilderFromFeedIo(): void
    {
        $feedItemBuilder = new Feed\FromFeedIo\ItemBuilder();

        $feedIoFeedItemAuthorMock = $this->createMock(\FeedIo\Feed\Item\Author::class);
        $feedIoFeedItemAuthorMock
            ->method('getName')
            ->willReturn('FeedIo\Feed\Item\Author');

        $feedIoFeedItemMock = $this->createMock(\FeedIo\Feed\Item::class);
        $feedIoFeedItemMock
            ->method('getTitle')
            ->willReturn('FeedIo\Feed\Item\Title');

        $feedIoFeedItemMock
            ->method('getContent')
            ->willReturn('<p>FeedIo\Feed\Item\Content</p>');

        $feedIoFeedItemMock
            ->method('getLink')
            ->willReturn('FeedIo\Feed\Item\Link');
        $pubDate = Carbon::now();

        $feedIoFeedItemMock
            ->method('getLastModified')
            ->willReturn($pubDate);

        $feedIoFeedItemMock
            ->method('getAuthor')
            ->willReturn($feedIoFeedItemAuthorMock);

        /** @var Item $feedItem */
        $feedItem = $feedItemBuilder->build($feedIoFeedItemMock);

        $this->assertTrue(is_a($feedItem, Item::class));

        $this->assertEquals($feedItem->getLink(), 'FeedIo\Feed\Item\Link');
        $this->assertEquals($feedItem->getCreator(), 'FeedIo\Feed\Item\Author');
        $this->assertEquals($feedItem->getDescription(), 'FeedIo\Feed\Item\Content');
        $this->assertEquals($feedItem->getTitle(), 'FeedIo\Feed\Item\Title');
        $this->assertTrue(is_a($feedItem->getPubDate(), Carbon::class));
        $this->assertEquals($feedItem->getPubDate(), $pubDate);
        $this->assertEquals($feedItem->getPubDate()->getTimestamp(), $pubDate->getTimestamp());

        /** @var Item $secondFeedItem */
        $secondFeedItem = $feedItemBuilder->getModel();

        $this->assertNotSame($feedItem, $secondFeedItem, 'Feed item builder is not reset model.');
    }
}
