<?php

namespace DStaroselskyi\RekrutacjaHRtec\Tests\Services\FilesGenerators\FeedCsv;

use Carbon\Carbon;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Item;
use DStaroselskyi\RekrutacjaHRtec\Services\FilesGenerators\FeedCsv\ColumnsStrategies\CreatorColumn;
use DStaroselskyi\RekrutacjaHRtec\Services\FilesGenerators\FeedCsv\ColumnsStrategies\DescriptionColumn;
use DStaroselskyi\RekrutacjaHRtec\Services\FilesGenerators\FeedCsv\ColumnsStrategies\LinkColumn;
use DStaroselskyi\RekrutacjaHRtec\Services\FilesGenerators\FeedCsv\ColumnsStrategies\PubDateColumn;
use DStaroselskyi\RekrutacjaHRtec\Services\FilesGenerators\FeedCsv\ColumnsStrategies\TitleColumn;
use PHPUnit\Framework\TestCase;

class FeedCsvColumnsStrategieTest extends TestCase
{
    protected function getFeedItem(): Item
    {
        /** @var Item $feedItem */
        $feedItem = new \DStaroselskyi\RekrutacjaHRtec\Models\Feed\Item();
        $feedItem->setCreator('<H1>Creator "Men"<H1>');
        $feedItem->setLink('https://tets.com?test=GGGG hhh+ppp');
        $feedItem->setDescription('<p>description</p>');
        $feedItem->setTitle('<h2>Title</h2>');

        return $feedItem;
    }

    public function testCreatorColumn(): void
    {
        $feedItem = $this->getFeedItem();

        $creatorColumn = new CreatorColumn();

        $this->assertEquals($creatorColumn->getTitle(), 'creator');
        $this->assertEquals($creatorColumn->getValue($feedItem), htmlentities('<H1>Creator "Men"<H1>'));
    }

    public function testDescriptionColumn(): void
    {
        $feedItem = $this->getFeedItem();

        $creatorColumn = new DescriptionColumn();

        $this->assertEquals($creatorColumn->getTitle(), 'description');
        $this->assertEquals($creatorColumn->getValue($feedItem), htmlentities('<p>description</p>'));
    }

    public function testTitleColumn(): void
    {
        $feedItem = $this->getFeedItem();

        $creatorColumn = new TitleColumn();

        $this->assertEquals($creatorColumn->getTitle(), 'title');
        $this->assertEquals($creatorColumn->getValue($feedItem), htmlentities('<h2>Title</h2>'));
    }

    public function testLinkColumn(): void
    {
        $feedItem = $this->getFeedItem();

        $creatorColumn = new LinkColumn();

        $this->assertEquals($creatorColumn->getTitle(), 'link');
        $this->assertEquals($creatorColumn->getValue($feedItem), rawurlencode('https://tets.com?test=GGGG hhh+ppp'));
    }

    public function testPubDateColumn(): void
    {
        $feedItem = $this->getFeedItem();
        $pubDate = Carbon::now();
        $feedItem->setPubDate($pubDate);

        $creatorColumn = new PubDateColumn();
        $dateString = $pubDate->locale('pl_PL')
            ->isoFormat('DD MMMM YYYY hh:mm:ss');

        $this->assertEquals($creatorColumn->getTitle(), 'pubDate');
        $this->assertEquals($creatorColumn->getValue($feedItem), $dateString);
    }
}
