<?php

namespace DStaroselskyi\RekrutacjaHRtec\Tests\Services\FilesGenerators\FeedCsv;

use DStaroselskyi\RekrutacjaHRtec\Contracts\Models\Feed\Item;
use DStaroselskyi\RekrutacjaHRtec\Contracts\Services\FilesGenerators\ColumnsStrategy;
use DStaroselskyi\RekrutacjaHRtec\Services\FilesGenerators\FeedCsv\FeedCsvStructure;
use PHPUnit\Framework\TestCase;

class FeedCsvStructureTest extends TestCase
{
    public function testFeedCsvStructure(): void
    {
        $columns = collect();
        for ($i = 1; $i < 6; $i++) {
            $columnMock = $this->createMock(ColumnsStrategy::class);
            $columnMock
                ->method('getTitle')
                ->willReturn('T' . $i);

            $columnMock
                ->method('getValue')
                ->willReturn('V' . $i);

            $columns->push($columnMock);
        }

        $feedCsvStructure = new FeedCsvStructure($columns);
        $feedItem = $this->createMock(Item::class);
        $this->assertEquals($feedCsvStructure->getTitleRow(), '"T1","T2","T3","T4","T5"');
        $this->assertEquals($feedCsvStructure->getRow($feedItem), '"V1","V2","V3","V4","V5"');
    }
}
