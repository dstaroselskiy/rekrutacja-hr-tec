<?php

namespace DStaroselskyi\RekrutacjaHRtec\Tests\Services\FilesGenerators\FeedCsv;

use DStaroselskyi\RekrutacjaHRtec\Models\Feed\Feed;
use DStaroselskyi\RekrutacjaHRtec\Services\FilesGenerators\FeedCsv\ExtendFeedToCsv;
use DStaroselskyi\RekrutacjaHRtec\Services\FilesGenerators\FeedCsv\FeedCsvStructure;
use DStaroselskyi\RekrutacjaHRtec\Services\FilesGenerators\FeedCsv\SaveFeedToCsv;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use PHPUnit\Framework\TestCase;

class FeedToCsvTest extends TestCase
{
    /**
     * @var vfsStreamDirectory
     */
    private $vfsStreamDirectory;

    /**
     * set up test environmemt.
     */
    public function setUp(): void
    {
        $this->vfsStreamDirectory = vfsStream::setup('feed');
    }

    public function testSaveFeedToCsv(): void
    {
        $feedCsvStructureMock = $this->createMock(FeedCsvStructure::class);
        $feedCsvStructureMock
            ->method('getTitleRow')
            ->willReturn('"T1","T2","T3","T4","T5"');

        $feedCsvStructureMock
            ->method('getRow')
            ->willReturn('"V1","V2","V3","V4","V5"');

        $saveFeedToCsv = new SaveFeedToCsv($feedCsvStructureMock);

        $feed = new Feed();
        for ($i = 1; $i < 5; $i++) {
            $feed->pushItem(new \DStaroselskyi\RekrutacjaHRtec\Models\Feed\Item());
        }
        $this->assertFalse($this->vfsStreamDirectory->hasChild('feed1.csv'));
        $saveFeedToCsv->save($feed, vfsStream::url('feed/feed1.csv'));
        $this->assertTrue($this->vfsStreamDirectory->hasChild('feed1.csv'));
        $this->assertEquals(
            file_get_contents($this->vfsStreamDirectory->getChild('feed1.csv')->url()),
            '"T1","T2","T3","T4","T5"
"V1","V2","V3","V4","V5"
"V1","V2","V3","V4","V5"
"V1","V2","V3","V4","V5"
"V1","V2","V3","V4","V5"
'
        );
        $saveFeedToCsv->save($feed, vfsStream::url('feed/feed1.csv'));
        $this->assertTrue($this->vfsStreamDirectory->hasChild('feed1.csv'));
        $this->assertEquals(
            file_get_contents($this->vfsStreamDirectory->getChild('feed1.csv')->url()),
            '"T1","T2","T3","T4","T5"
"V1","V2","V3","V4","V5"
"V1","V2","V3","V4","V5"
"V1","V2","V3","V4","V5"
"V1","V2","V3","V4","V5"
'
        );

        for ($i = 1; $i < 5; $i++) {
            $feed->pushItem(new \DStaroselskyi\RekrutacjaHRtec\Models\Feed\Item());
        }
        $saveFeedToCsv->save($feed, vfsStream::url('feed/feed1.csv'));
        $this->assertTrue($this->vfsStreamDirectory->hasChild('feed1.csv'));
        $this->assertEquals(
            file_get_contents($this->vfsStreamDirectory->getChild('feed1.csv')->url()),
            '"T1","T2","T3","T4","T5"
"V1","V2","V3","V4","V5"
"V1","V2","V3","V4","V5"
"V1","V2","V3","V4","V5"
"V1","V2","V3","V4","V5"
"V1","V2","V3","V4","V5"
"V1","V2","V3","V4","V5"
"V1","V2","V3","V4","V5"
"V1","V2","V3","V4","V5"
'
        );
    }

    public function testExtendFeedToCsvFileNotExist(): void
    {
        $feedCsvStructureMock = $this->createMock(FeedCsvStructure::class);
        $feedCsvStructureMock
            ->method('getTitleRow')
            ->willReturn('"T1","T2","T3","T4"');

        $feedCsvStructureMock
            ->method('getRow')
            ->willReturn('"V1","V2","V3","V4"');

        $saveFeedToCsv = new ExtendFeedToCsv($feedCsvStructureMock);

        $feed = new Feed();
        for ($i = 1; $i < 3; $i++) {
            $feed->pushItem(new \DStaroselskyi\RekrutacjaHRtec\Models\Feed\Item());
        }
        $this->assertFalse($this->vfsStreamDirectory->hasChild('feed2.csv'));
        $saveFeedToCsv->save($feed, vfsStream::url('feed/feed2.csv'));
        $this->assertTrue($this->vfsStreamDirectory->hasChild('feed2.csv'));
        $this->assertEquals(
            file_get_contents($this->vfsStreamDirectory->getChild('feed2.csv')->url()),
            '"T1","T2","T3","T4"
"V1","V2","V3","V4"
"V1","V2","V3","V4"
'
        );
        $saveFeedToCsv->save($feed, vfsStream::url('feed/feed2.csv'));
        $this->assertTrue($this->vfsStreamDirectory->hasChild('feed2.csv'));
        $this->assertEquals(
            file_get_contents($this->vfsStreamDirectory->getChild('feed2.csv')->url()),
            '"T1","T2","T3","T4"
"V1","V2","V3","V4"
"V1","V2","V3","V4"
"V1","V2","V3","V4"
"V1","V2","V3","V4"
'
        );
    }

    public function testExtendFeedToCsvFileExist(): void
    {
        vfsStream::newFile('feed3.csv')
            ->withContent('"T1","T2","T3"
"V1","V2","V3"
"V1","V2","V3"
')
            ->at($this->vfsStreamDirectory);

        $feedCsvStructureMock = $this->createMock(FeedCsvStructure::class);
        $feedCsvStructureMock
            ->method('getTitleRow')
            ->willReturn('"T1","T2","T3"');

        $feedCsvStructureMock
            ->method('getRow')
            ->willReturn('"V1","V2","V3"');

        $saveFeedToCsv = new ExtendFeedToCsv($feedCsvStructureMock);

        $feed = new Feed();
        for ($i = 1; $i < 3; $i++) {
            $feed->pushItem(new \DStaroselskyi\RekrutacjaHRtec\Models\Feed\Item());
        }
        $this->assertTrue($this->vfsStreamDirectory->hasChild('feed3.csv'));
        $this->assertEquals(
            file_get_contents($this->vfsStreamDirectory->getChild('feed3.csv')->url()),
            '"T1","T2","T3"
"V1","V2","V3"
"V1","V2","V3"
'
        );
        $saveFeedToCsv->save($feed, vfsStream::url('feed/feed3.csv'));
        $this->assertTrue($this->vfsStreamDirectory->hasChild('feed3.csv'));

        $this->assertEquals(
            file_get_contents($this->vfsStreamDirectory->getChild('feed3.csv')->url()),
            '"T1","T2","T3"
"V1","V2","V3"
"V1","V2","V3"
"V1","V2","V3"
"V1","V2","V3"
'
        );
    }
}
